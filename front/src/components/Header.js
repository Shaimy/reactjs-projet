import React, {Component} from 'react';
import {Link} from 'react-router-dom';


class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isAuth: false,
            admin: false,
            _id: ''
        }
    }

    async componentDidMount() {
        /* Si un token existe */
        localStorage.getItem('token') && this.setState({isAuth: true});

        /* Récupérer le contenu du token pour savoir si c'est un admin ou pas */
        let role = localStorage.getItem('token');
        if (role === 'trueAdmin') {
            this.setState({admin: true});
        } else {
            this.setState({admin: false});
        }
        /* Récupérer id du client */
        let idUser = localStorage.getItem('_id');
        this.setState({
            _id: idUser
        });
    }

    logout(e) {
        e.preventDefault();
        localStorage.removeItem('token');
        localStorage.removeItem('_id');
        this.setState({isAuth: false});
        window.location.href = "/login";
    }

    render(){
        let {admin, isAuth, _id} = this.state;
        return(
            <nav className="navbar navbar-expand-lg navbar-light bg-color-nav">
                <Link className="nav-link logo" to={'/'}>Veigar OS</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                {
                    admin && isAuth?
                    (
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to={'/add'}>Ajout d'un jeu</Link>
                            </li>
                            <li className="nav-item active">
                                <Link className="nav-link" to={'/users-list'}>Utilisateurs</Link>
                            </li>
                            <li className="nav-item active">
                                <a className="nav-link" onClick={(e) => this.logout(e)} href="">Deconnexion</a>
                            </li>
                        </ul>
                    ): !admin && isAuth ?
                    (
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to={`/users/${_id}`}>Profil</Link>
                            </li>
                            <li className="nav-item active">
                                <a className="nav-link" onClick={(e) => this.logout(e)} href="">Deconnexion</a>
                            </li>
                        </ul> 
                    ):
                    (
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to={'/register'}>Inscription</Link>
                            </li>
                            <li className="nav-item active">
                                <Link className="nav-link" to={'/login'}>Connexion</Link>
                            </li>
                        </ul> 
                    )
                }
                </div>
            </nav>
        )
    }
}

export default Header;