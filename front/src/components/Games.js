import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../App.css';

class Games extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        let {_id, name, image1} = this.props.data;
        return (
            <div className="col">
                <br></br>
                <p className="titleGames">{name}</p>
                <Link to={`/games/${_id}`}><img className="cropped center widthGames" src={`img/${image1}`} alt={name} /></Link>
            </div>
        )
    }
}

export default Games;