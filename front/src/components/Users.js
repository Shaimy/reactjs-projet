import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Users extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {_id, firstname, lastname, email, user_role} = this.props.data;
        return (
            <tr>
                <th>{lastname}</th>
                <th>{firstname}</th>
                <th>{email}</th>
                <th>
                <Link className="btn btn-warning" style={{marginRight: "10px"}} to={`/users/update/${_id}`}>Modifier</Link>
                {
                    user_role === 2 &&
                        <button className="btn btn-danger" onClick={() => this.props.delete(_id)}>Supprimer</button>
                }
                </th>
            </tr>
        )
    }
}

export default Users;