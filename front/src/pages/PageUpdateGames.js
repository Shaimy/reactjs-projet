import React, {Component} from 'react';
import GamesService from '../services/GamesService';

class PageUpdateGames extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            image1: '',
            image2: '',
            platform: ''
        }
    }

    async componentDidMount(){
        let {id} = this.props.match.params;
        let response = await GamesService.details(id);

        if (response.ok) {
            let data = await response.json();
            this.setState({
                name: data.games.name, 
                description: data.games.description,
                image1: data.games.image1,
                image2: data.games.image2,
                platform: data.games.platform
            })
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e) {
        e.preventDefault();
        let {id} = this.props.match.params;
        let {name, description, image1, image2, platform} = this.state;

        let body = {
            name,
            description,
            image1,
            image2,
            platform
        };

        let response = await GamesService.update(id, body);

        if (response.ok) {
            this.props.history.push('/');
        }
    }

    render(){
        let {name, description, image1, image2, platform} = this.state;
        return(
            <div className="container">
                <h3>Modification</h3>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Nom</label>
                        <input id="name" required className="form-control" onChange={(e) => this.handleChange(e)} value={name}/>
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <input id="description" required className="form-control" onChange={(e) => this.handleChange(e)} value={description}/>
                    </div>
                    <div className="form-group">
                        <label>Image d'accueil</label>
                        <input id="image1" required className="form-control" onChange={(e) => this.handleChange(e)} value={image1}/>
                    </div>
                    <div className="form-group">
                        <label>Image du détails</label>
                        <input id="image2" required className="form-control" onChange={(e) => this.handleChange(e)} value={image2}/>
                    </div>
                    <div className="form-group">
                        <label>Plate-forme</label>
                        <input id="platform" required className="form-control" onChange={(e) => this.handleChange(e)} value={platform}/>
                    </div>
                    <button type="submit" className="btn btn-warning">Modifier</button>
                </form>
            </div>
        )
    }
}

export default PageUpdateGames;