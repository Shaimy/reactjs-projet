import React, { Component } from 'react';
import UsersService from '../services/UsersService';
import FavoriteService from '../services/FavoriteService';
import {Link} from 'react-router-dom';
import '../App.css'

class PageProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: null,
            favorite: []
        };
    }

    /* Récupération des données user et lister ses jeux en favoris */
    async componentDidMount(){
        let {id} = this.props.match.params;
        let response = await UsersService.details(id);

        if (response.ok) {
            let data = await response.json();
            this.setState({
                user: data.users
            });
        }

        let favorite = await FavoriteService.list();
        if (favorite.ok) {
            let data = await favorite.json()
            this.setState({
                favorite: data.fav
            });
        }
    }

    /* RGPD : Suppression le compte */
    async delete(id) {
        let response = await UsersService.delete(id);
        if (response.ok) {
            /* Supprimer les favoris qui vont avec l'user */
            this.state.favorite.map(async (item) => {
                if (item.idUser._id === id)
                {
                    let removefav = await FavoriteService.delete(item._id);
                    if (removefav.ok) {
                        localStorage.removeItem('token');
                        localStorage.removeItem('_id');
                        this.props.history.push('/');
                        window.location.reload();
                    }
                }
            })
        }   
    }

    /* Suppression d'un jeu dans vos favoris */
    async deleteFav(idGame) {
        let response = await FavoriteService.delete(idGame);
        if (response.ok) {
            window.location.reload();
        }
    }

    render() {
        let {user} = this.state;
        return(
            <div className="container">
                <h3>Profil</h3>
                <div className="row">
                    <div className="col mywidth">
                        <h5>{user && user.firstname} {user && user.lastname}</h5>
                        <p>Email : {user && user.email}</p>
                        <p>
                            <a className="btn btn-warning" href={`/users/update/${user && user._id}`}>Modifier</a>
                            <button className="btn btn-danger" style={{ marginLeft: '1em' }} onClick={() => this.delete(user && user._id)}>Supprimer votre compte</button>
                        </p>

                    </div>
                    
                </div>
                <br></br>
                <h5>Vos favoris</h5>
                <div className="row">
                        {
                            // Récuperer les jeux en favoris en fonction du client
                            this.state.favorite.map((item) => {
                                if (item.idUser._id === user._id)
                                {
                                    return (
                                        <div className="col-sm">
                                            <p>{item.idGame.name}</p>
                                            <Link to={`/games/${item.idGame._id}`}><img className="cropped widthGames" src={`../img/${item.idGame.image1}`} alt={item.idGame.name} /></Link>
                                            <br/><br/>
                                            <button className="btn btn-danger" onClick={() => this.deleteFav(item._id)}>Supprimer</button>
                                        </div>
                                    )
                                } else {
                                    return (
                                        <p></p>
                                    )
                                }      
                            })
                        }
                </div>
            </div>
        )
    }
}

export default PageProfile;