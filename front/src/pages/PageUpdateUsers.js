import React, {Component} from 'react';
import UsersService from '../services/UsersService';

class PageUpdateUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: ''
        }
    }

    async componentDidMount(){
        let {id} = this.props.match.params;
        let response = await UsersService.details(id);

        if (response.ok) {
            let data = await response.json();
            this.setState({
                firstname: data.users.firstname, 
                lastname: data.users.lastname,
                email: data.users.email,
                password: data.users.password
            })
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e) {
        e.preventDefault();
        let {id} = this.props.match.params;
        let {firstname, lastname, email, password} = this.state;

        let body = {
            firstname,
            lastname,
            email,
            password
        };

        let response = await UsersService.update(id, body);
        if (response.ok) {
            this.props.history.push('/');
        }
    }

    render(){
        let {firstname, lastname, email, password} = this.state;
        return(
            <div className="container">
                <h3>Modification</h3>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Prénom</label>
                        <input id="firstname" required className="form-control" onChange={(e) => this.handleChange(e)} value={firstname}/>
                    </div>
                    <div className="form-group">
                        <label>Nom</label>
                        <input id="lastname" required className="form-control" onChange={(e) => this.handleChange(e)} value={lastname}/>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input id="email" required className="form-control" onChange={(e) => this.handleChange(e)} value={email}/>
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input id="password" required className="form-control" onChange={(e) => this.handleChange(e)} value={password}/>
                    </div>
                    <button type="submit" className="btn btn-warning">Modifier</button>
                </form>
            </div>
        )
    }
}

export default PageUpdateUsers;