import React, { Component } from 'react';
import GamesService from '../services/GamesService';
import FavoriteService from '../services/FavoriteService';

class PageDetailsGames extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isAuth: false,
            admin: false,
            game: null
        };
    }

    /* Récupération des données */
    async componentDidMount(){
        /* Si un token existe */
        localStorage.getItem('token') && this.setState({isAuth: true});

        /* Récupérer le contenu du token pour savoir si c'est un admin ou pas */
        let role = localStorage.getItem('token');
        if (role === 'trueAdmin') {
            this.setState({admin: true});
        } else {
            this.setState({admin: false});
        }

        /* Details des jeux */
        let {id} = this.props.match.params;
        let response = await GamesService.details(id);

        if (response.ok) {
            let data = await response.json();
            this.setState({
                game: data.games
            });
        }

    }

    /* Suppression d'un jeu */
    async delete(id) {
        let response = await GamesService.delete(id);
        if (response.ok) {
            this.props.history.push('/');
        }
    }

    /* Ajout en favoris */
    async add() {
        let idUser = localStorage.getItem('_id');
        let idGame = this.state.game._id;
        let body = {
            idUser,
            idGame
        }

        let response = await FavoriteService.add(body);
        if (response.ok) {
            this.props.history.push('/');
        }
    }

    render() {
        let {game, isAuth, admin} = this.state;
        return(
            <div className="container">
                <h3>Détails du jeu</h3>
                <div className="row">
                    <div className="col">
                        <img className="cropped widthDetailsGames" src={`/img/${game && game.image2}`} alt={game && game.name} />
                    </div>
                    <div className="col align-self-center">
                        <h5>{game && game.name}</h5>
                        <p>{game && game.description}</p>
                        <p className="platform">Plate-forme : {game && game.platform}</p>
                        {
                            isAuth && admin ?
                            (
                                <p>
                                    <a className="btn btn-warning" href={`/games/update/${game && game._id}`}>Modifier</a>
                                    <button className="btn btn-danger" style={{ marginLeft: '1em' }} onClick={() => this.delete(game && game._id)}>Supprimer</button>
                                </p>
                            ) : !admin && isAuth ?
                            (
                                <p><button className="btn btn-success" style={{ marginLeft: '1em' }} onClick={() => this.add()}>Ajouter dans vos favoris</button></p>
                            ) :
                            (
                                <p></p>
                            )
                        }
                        
                    </div>
                </div>
            </div>
        )
    }
}

export default PageDetailsGames;