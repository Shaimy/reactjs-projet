import React, { Component } from 'react';
import UsersService from '../services/UsersService';
import Users from '../components/Users';

class PageUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    async componentDidMount() {
        let response = await UsersService.list();
        if (response.ok) {
            let data = await response.json();
            this.setState({
                users: data.users
            });
        }
    }

    render() {
        return(
            <div className="container">
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.users.map(item => {
                            return (
                                <Users data={item} />
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default PageUsers;