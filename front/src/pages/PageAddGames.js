import React, {Component} from 'react';
import GamesService from '../services/GamesService';

class PageAddGames extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            image1: '',
            image2: '',
            platform: ''
        }
    }

    componentDidMount() {
        let role = localStorage.getItem('token');
        if (role !== 'trueAdmin') {
           this.props.history.push('/');
        }
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e) {
        e.preventDefault();
        let {name, description, image1, image2, platform} = this.state;

        let body = {
            name,
            description,
            image1,
            image2,
            platform
        };

        let response = await GamesService.add(body);

        if (response.ok) {
            this.props.history.push('/');
        }
    }

    render(){
        let {name, description, image1, image2, platform} = this.state;
        return(
            <div className="container">
                <h3>Ajout d'un jeu </h3>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Nom</label>
                        <input id="name" required className="form-control" onChange={(e) => this.handleChange(e)} value={name}/>
                    </div>
                    <div className="form-group">
                        <label>Description</label>
                        <input id="description" required className="form-control" onChange={(e) => this.handleChange(e)} value={description}/>
                    </div>
                    <div className="form-group">
                        <label>Image d'accueil</label>
                        <input id="image1" required className="form-control" onChange={(e) => this.handleChange(e)} value={image1}/>
                    </div>
                    <div className="form-group">
                        <label>Image du détails</label>
                        <input id="image2" required className="form-control" onChange={(e) => this.handleChange(e)} value={image2}/>
                    </div>
                    <div className="form-group">
                        <label>Plate-forme</label>
                        <input id="platform" required className="form-control" onChange={(e) => this.handleChange(e)} value={platform}/>
                    </div>
                    <button type="submit" className="btn btn-info">Ajouter</button>
                </form>
            </div>
        )
    }
}

export default PageAddGames;