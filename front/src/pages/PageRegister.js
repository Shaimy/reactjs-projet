import React, {Component} from 'react';
import UsersService from '../services/UsersService';


class PageRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: ''
        }
    }

   handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e) {
        e.preventDefault();
        let {lastname, firstname, email, password} = this.state;
        let body = {
            firstname,
            lastname,
            email,
            password
        };

        let response = await UsersService.add(body);

        if (response.ok) {
            this.props.history.push('/');
        }
    }
    render(){
        let {lastname, firstname, email, password} = this.state;
        return(
            <div className="container">
                <h3>Inscription</h3>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Nom</label>
                        <input id="lastname" required className="form-control" onChange={(e) => this.handleChange(e)} value={lastname}/>
                    </div>
                    <div className="form-group">
                        <label>Prenom</label>
                        <input id="firstname" required className="form-control" onChange={(e) => this.handleChange(e)} value={firstname}/>
                    </div>
                    <div className="form-group">
                        <label>Email</label>
                        <input id="email" required className="form-control" onChange={(e) => this.handleChange(e)} value={email}/>
                    </div>
                    <div className="form-group">
                        <label>Mot de passe</label>
                        <input type="password" id="password" required className="form-control" onChange={(e) => this.handleChange(e)} value={password}/>
                    </div>
                    <button type="submit" className="btn btn-info">Inscription</button>
                </form>
            </div>
        )
    }
}

export default PageRegister;
