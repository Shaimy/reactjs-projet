import React, { Component } from 'react';
import GamesService from '../services/GamesService';
import Games from '../components/Games';

class PageGames extends Component {

    constructor(props) {
        super(props);
        this.state = {
            games: []
        };
    }

    async componentDidMount() {
        let response = await GamesService.list();
        if (response.ok) {
            let data = await response.json();
            console.log(data);
            this.setState({
                games: data.games
            });
        }
    }

    render() {
        return(
            <div className="container">
                <div className="row">
                {
                    this.state.games.map(item => {
                        return (
                            <Games data={item}/>
                        )
                    })
                }
                </div>
            </div>
        )
    }
}

export default PageGames;