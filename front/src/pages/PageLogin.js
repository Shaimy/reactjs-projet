import React, {Component} from 'react';
import UsersService from '../services/UsersService';


class PageLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

   handleChange(e){
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async submit(e) {
        e.preventDefault();
        let {email, password} = this.state;
        let body = {
            email,
            password
        };

        let response = await UsersService.auth(body);

        if (response.ok) {
            let data = await response.json();
            /* Création d'un token en dur pour différencier un client et un admin */
            if (data.users.user_role === 1) {
                localStorage.setItem('token', 'trueAdmin');
                localStorage.setItem('_id', data.users._id);
                
            } else {
                localStorage.setItem('token', 'trueClient');
                localStorage.setItem('_id', data.users._id);
            }
            this.props.history.push('/');
            window.location.reload();
        }
    }

    render(){
        let {email, password} = this.state;
        return(
            <div className="container">
                <h3>Connexion</h3>
                <form onSubmit={(e) => this.submit(e)}>
                    <div className="form-group">
                        <label>Email</label>
                        <input id="email" required className="form-control" onChange={(e) => this.handleChange(e)} value={email}/>
                    </div>
                    <div className="form-group">
                        <label>Mot de passe</label>
                        <input type="password" id="password" required className="form-control" onChange={(e) => this.handleChange(e)} value={password}/>
                    </div>
                    <button type="submit" className="btn btn-info">Connexion</button>
                </form>
            </div>
        )
    }
}

export default PageLogin;
