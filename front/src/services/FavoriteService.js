const baseUrl = "http://localhost:3030"; // url API

class FavoriteService {
    
    static async list() {

        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${baseUrl}/favorite`, init);
        return call;
    }

    static async add(body) {
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        }
        let call = await fetch(`${baseUrl}/favorite`, init);
        return call;
    }

    static async delete(id) {
        let init = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }
        let call = await fetch(`${baseUrl}/favorite/${id}`, init);
        return call;
    }

}

export default FavoriteService;