import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Games from './pages/PageGames';
import DetailsGames from './pages/PageDetailsGames';
import AddGames from './pages/PageAddGames';
import Header from './components/Header';
import UpdateGames from './pages/PageUpdateGames';
import Register from './pages/PageRegister';
import Login from './pages/PageLogin';
import Profile from './pages/PageProfile';
import UpdateProfile from './pages/PageUpdateUsers';
import UsersList from './pages/PageUsers';

class App extends Component {
    render() {
        return(
            <BrowserRouter>
            <Header></Header>
                <Route path="/" exact component={Games} />
                <Route path="/games/:id" exact component={DetailsGames} />
                <Route path="/add" exact component={AddGames} />
                <Route path="/games/update/:id" exact component={UpdateGames} />
                <Route path="/register" exact component={Register} />
                <Route path="/login" exact component={Login} />
                <Route path="/users/:id" exact component={Profile} />
                <Route path="/users/update/:id" exact component={UpdateProfile} />
                <Route path="/users-list" exact component={UsersList} />
            </BrowserRouter>
        )
    }
}

export default App;