import express from 'express';
import database from './src/models/database';
import router from './src/routes/routes';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();
const port = 3030;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors({origin: true}));

//Utiliser la route dans src
app.use(router);

database.connectDb().then(() => {
    console.log('Database connecting');
    app.listen(port, () => {
        console.log(`server listening ${port}`);
    });
});