import Users from "../models/users";

class UsersController {

    /*  Afficher la liste des utilisateurs */
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let users = await Users.find();

            body = {
                users,
                'message': 'Listes des utilisateurs'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Creation d'un utilisateur */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {
            let users = await Users.create({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password
            });

            body = {
                users,
                'message': 'Creation utilisateur : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Modifier les informations d'un utilisateur */
    static async update(req, res) {
        let status = 200;
        let body = {};

        try {
            let users = await Users.findByIdAndUpdate(req.params.myid, req.body, { new: true });

            body = {
                users,
                'message': 'Modification utilisateur : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Supprimer un utilisateur */
    static async delete(req, res) {
        let status = 200;
        let body = {};

        try {
            let id = req.params.myid;
            await Users.findByIdAndRemove(id);

            body = {
                'message': 'Suppression utilisateur : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Afficher les détails */
    static async details(req, res) {
        let status = 200;
        let body = {};

        try {
            let id = req.params.myid;
            let users = await Users.findById(id);

            body = {
                users,
                'message': 'Détails utilisateur'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    static async auth(req, res) {
        let status = 200;
        let body = {};
        try {
            //Checker si l'utilisateur a le bon mot de passe et email
            let users = await Users.findOne({ email: req.body.email });
            if (users && users.password === req.body.password) {
                body = {
                    users,
                    'message': 'Connexion réussie'
                };
            } else {
                status = 401;
                body = { 'message': 'Erreur email ou mot de passe' };
            }
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

}

export default UsersController;