import Favorite from "../models/favorite";

class FavoriteController {

    /*  Afficher la liste des jeux en favoris */
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let fav = await Favorite.find().populate('idUser').populate('idGame');

            body = {
                fav,
                'message': 'Liste des jeux en favoris'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Creation */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {
            let fav = await Favorite.create({
                idUser: req.body.idUser,
                idGame: req.body.idGame
            });

            body = {
                fav,
                'message': 'Favoris créé'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Suppression */
    static async delete(req, res) {
        let status = 200;
        let body = {};

        try {
            let idfav = req.params.myid;
            await Favorite.findByIdAndRemove(idfav);

            body = {
                'message': 'Suppression du jeu en favoris : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

}

export default FavoriteController;