import Roles from "../models/roles";

class RolesController {

    /*  Afficher la liste des roles */
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let roles = await Roles.find();

            body = {
                roles,
                'message': 'Liste des roles'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Creation d'un role */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {
            let roles = await Roles.create({
                idRole: req.body.idRole,
                name: req.body.name
            });

            body = {
                roles,
                'message': 'Creation du role : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Modifier le role */
    static async update(req, res) {
        let status = 200;
        let body = {};

        try {
            let roles = await Roles.findByIdAndUpdate(req.params.myid, req.body, { new: true });

            body = {
                roles,
                'message': 'Modification du role : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Supprimer un role */
    static async delete(req, res) {
        let status = 200;
        let body = {};

        try {
            let id = req.params.myid;
            await Roles.findByIdAndRemove(id);

            body = {
                'message': 'Supression du role : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

}

export default RolesController;