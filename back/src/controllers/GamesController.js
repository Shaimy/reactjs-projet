import Games from "../models/games";

class GamesController {

    /* Lister les jeux de la database */
    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let games = await Games.find();
            body = {
                games,
                'message': 'Liste des jeux'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Creation d'un jeu dans la database */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {
            let games = await Games.create({
                name: req.body.name,
                description: req.body.description,
                image1: req.body.image1,
                image2: req.body.image2,
                platform: req.body.platform
            });

            body = {
                games,
                'message': 'Création du jeu : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

    /* Modification */
    static async update(req, res) {
        let status = 200;
        let body = {};

        try {
            let idgame = req.params.myid;
            let games = await Games.findByIdAndUpdate(idgame, req.body, { new: true });

            body = {
                games,
                'message': 'Modification du jeu : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }
    /* Suppression */
    static async delete(req, res) {
        let status = 200;
        let body = {};

        try {
            let idgame = req.params.myid;
            await Games.findByIdAndRemove(idgame);

            body = {
                'message': 'Suppression du jeu : OK'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }
    /* Détails du jeu */
    static async details(req, res) {
        let status = 200;
        let body = {};

        try {
            let id = req.params.myid;
            let games = await Games.findById(id);

            body = {
                games,
                'message': 'Details du jeu'
            };
        } catch (error) {
            status = 500;
            body = { 'message': error.message };
        }
        return res.status(status).json(body);
    }

}

export default GamesController;