import mongoose from 'mongoose';

// Création du schema 
const rolesSchema = new mongoose.Schema({
    idRole: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

const Roles = mongoose.model('Roles', rolesSchema);
export default Roles;