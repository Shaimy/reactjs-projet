import mongoose from 'mongoose';

// Création du schema 
const favoriteSchema = new mongoose.Schema({
    idUser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    idGame: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Games'
    }
});

const Favorite = mongoose.model('Favorite', favoriteSchema);

export default Favorite;