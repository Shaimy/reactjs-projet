import mongoose from 'mongoose';

/* Création et connexion à la base  */
const connectDb = () => {
    let connection = null;

    connection = mongoose.connect('mongodb://localhost:27017/game', {
        useNewUrlParser: true,
        UseUnifiedTopology: true
    });
    return connection;
}

export default { connectDb };