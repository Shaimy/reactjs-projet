import mongoose from 'mongoose';

// Création du schema 
const usersSchema = new mongoose.Schema({

    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    user_role: {
        type: Number,
        default: 2
    }
});

const Users = mongoose.model('Users', usersSchema);
export default Users;