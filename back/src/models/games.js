import mongoose from 'mongoose';

// Création du schema 
const gamesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image1: {
        type: String,
        required: true
    },
    image2: {
        type: String,
        required: true
    },
    platform: {
        type: String,
        required: true
    }
});

const Games = mongoose.model('Games', gamesSchema);

export default Games;