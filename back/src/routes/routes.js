import { Router } from 'express';
import GamesController from '../controllers/GamesController';
import UsersController from '../controllers/UsersController';
import RolesController from '../controllers/RolesController';
import FavoriteController from '../controllers/FavoriteController';

const router = Router();

//Routes
router.get('/games', GamesController.list);
router.post('/games', GamesController.create);
router.put('/games/update/:myid', GamesController.update);
router.delete('/games/:myid', GamesController.delete);
router.get('/games/:myid', GamesController.details);

router.get('/users', UsersController.list);
router.post('/users', UsersController.create);
router.put('/users/update/:myid', UsersController.update);
router.delete('/users/:myid', UsersController.delete);
router.get('/users/:myid', UsersController.details);
router.post('/users/auth', UsersController.auth);

router.get('/roles', RolesController.list);
router.post('/roles', RolesController.create);
router.put('/roles/update/:myid', RolesController.update);
router.delete('/roles/:myid', RolesController.delete);

router.get('/favorite', FavoriteController.list);
router.post('/favorite', FavoriteController.create);
router.delete('/favorite/:myid', FavoriteController.delete);

export default router;